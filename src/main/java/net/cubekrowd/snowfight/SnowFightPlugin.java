package net.cubekrowd.snowfight;

import com.destroystokyo.paper.event.player.PlayerLaunchProjectileEvent;
import com.destroystokyo.paper.event.server.ServerTickEndEvent;
import com.google.common.collect.HashMultimap;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.LocationFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagConflictException;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.logging.Level;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Snowman;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.RenderType;
import org.jetbrains.annotations.NotNull;

public class SnowFightPlugin extends JavaPlugin implements Listener {
    public StateFlag snowFightFlag;
    public StateFlag snowFightKillZoneFlag;
    public LocationFlag snowFightSpawnFlag;
    // present for all online players
    public Map<Player, PlayerData> playerData;
    public ItemStack snowballItem;
    public NamespacedKey snowballId = new NamespacedKey(this, "snowball");
    public ItemStack crossbowItem;
    public NamespacedKey crossbowId = new NamespacedKey(this, "crossbow");
    public NamespacedKey fireworkId = new NamespacedKey(this, "firework");
    public NamespacedKey bootsId = new NamespacedKey(this, "boots");
    // retained until plugin disabled, players inserted when they join
    public Map<UUID, PlayerScores> scoresMap;
    // extra snowball data
    public Map<Snowball, SnowballData> snowballData;
    public Map<Snowman, Integer> spawnedSnowmen;

    public String enterTitle = ChatColor.AQUA + "Entering The Blizzard";
    public String enterSubtitle = ChatColor.WHITE + "Don't get hit by snowballs!";
    public String exitTitle = ChatColor.RED + "Leaving The Blizzard";
    public String exitSubtitle = "";
    public List<String> snowballDeathMessages = List.of(
        ChatColor.AQUA + "{0}" + ChatColor.WHITE + " received a fluffy present from " + ChatColor.BLUE + "{1}" + ChatColor.WHITE + "!",
        ChatColor.AQUA + "{0}" + ChatColor.WHITE + " was buried in snow by " + ChatColor.BLUE + "{1}" + ChatColor.WHITE + "!",
        ChatColor.AQUA + "{0}" + ChatColor.WHITE + " got avalanche''d by " + ChatColor.BLUE + "{1}" + ChatColor.WHITE + "!"
    );
    public List<String> fireworkDeathMessages = List.of(
        ChatColor.AQUA + "{0}" + ChatColor.WHITE + " was blasted into the new year by " + ChatColor.BLUE + "{1}" + ChatColor.WHITE + "!",
        ChatColor.AQUA + "{0}" + ChatColor.WHITE + " couldn''t handle the New Year''s resolutions of " + ChatColor.BLUE + "{1}" + ChatColor.WHITE + "!"
    );
    public Random random = new Random();

    public String dbFileName = "data.db";
    public volatile boolean busySavingToDB;
    public boolean saveToDBOnExit;
    public boolean dataChangedSinceLastSave;

    public int killZoneExitDelay = 5 * 20;

    public boolean newYearMode;

    public int minKillsForAdvancement = 5;

    public <T extends Flag> T registerFlag(T flag) {
        var flagRegistry = WorldGuard.getInstance().getFlagRegistry();
        var res = flag;
        try {
            flagRegistry.register(res);
        } catch (FlagConflictException | IllegalStateException e) {
            var existingFlag = flagRegistry.get(flag.getName());
            if (flag.getClass().isInstance(existingFlag)) {
                res = (T) existingFlag;
            }
        }
        return res;
    }

    @Override
    public void onLoad() {
        snowFightFlag = registerFlag(new StateFlag("snowfight", false));
        snowFightKillZoneFlag = registerFlag(new StateFlag("snowfight-killzone", false));
        snowFightSpawnFlag = registerFlag(new LocationFlag("snowfight-spawn"));
    }

    @Override
    public void onEnable() {
        if (snowFightFlag == null || snowFightKillZoneFlag == null) {
            // too bad
            getLogger().severe("flag already registered by someone else");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        scoresMap = new HashMap<>();

        playerData = new HashMap<>();

        snowballItem = new ItemStack(Material.SNOWBALL);
        var im = snowballItem.getItemMeta();
        var data = im.getPersistentDataContainer();
        data.set(snowballId, PersistentDataType.BYTE, (byte) 0);
        snowballItem.setItemMeta(im);

        crossbowItem = new ItemStack(Material.CROSSBOW);
        crossbowItem.addEnchantment(Enchantment.QUICK_CHARGE, 3);
        im = crossbowItem.getItemMeta();
        im.setUnbreakable(true);
        im.addItemFlags(ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ATTRIBUTES);
        data = im.getPersistentDataContainer();
        data.set(crossbowId, PersistentDataType.BYTE, (byte) 0);
        crossbowItem.setItemMeta(im);

        // weak hash map so we don't have to remove entries explicitly
        snowballData = new WeakHashMap<>();
        spawnedSnowmen = new WeakHashMap<>();

        // initialise existing players in case this plugin was reloaded or
        // something
        for (var p : Bukkit.getOnlinePlayers()) {
            playerData.put(p, new PlayerData());
        }

        if (!loadData()) {
            saveToDBOnExit = false;
        }

        saveToDBOnExit = true;
        busySavingToDB = false;

        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        if (saveToDBOnExit && !busySavingToDB) {
            getLogger().info("Saving data");
            dumpData(serialiseData());
        }

        for (var p : Bukkit.getOnlinePlayers()) {
            p.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
        }

        snowFightFlag = null;
        snowFightKillZoneFlag = null;
        playerData = null;
        scoresMap = null;
        snowballData = null;
        for (var snowman : spawnedSnowmen.keySet()) {
            snowman.remove();
        }
        spawnedSnowmen = null;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length == 0) {
            sender.sendMessage("Not enough arguments");
            return true;
        }
        var sub = args[0].toLowerCase(Locale.ENGLISH);
        if (sub.equals("newyear")) {
            var newMode = !newYearMode;
            setNewYearMode(newMode);
            sender.sendMessage("Set New Year mode to " + newMode);
        } else if (sub.equals("save")) {
            sender.sendMessage("Saving data");
            scheduleSaveData(true);
        } else if (sub.equals("setkills")) {
            if (args.length < 3) {
                sender.sendMessage("Not enough arguments");
            } else {
                var target = Bukkit.getPlayer(args[1]);
                var newScore = Integer.parseInt(args[2]);
                var ps = scoresMap.computeIfAbsent(target.getUniqueId(), k -> new PlayerScores());
                sender.sendMessage("Set kills from " + ps.kills + " to " + newScore + " for " + target.getName());
                ps.kills = newScore;
                dataChangedSinceLastSave = true;
            }
        } else {
            sender.sendMessage("Unknown subcommand: " + sub);
        }
        return true;
    }

    public byte[] serialiseData() {
        // @NOTE(traks) just allocate a big buffer to write to. Should be large
        // enough for our needs
        var buf = ByteBuffer.allocate(1 << 20);

        int dataVersion = 1;
        buf.putInt(dataVersion);
        buf.putInt(newYearMode ? 1 : 0);
        for (var entry : scoresMap.entrySet()) {
            var uuid = entry.getKey();
            var ps = entry.getValue();
            buf.putLong(uuid.getMostSignificantBits());
            buf.putLong(uuid.getLeastSignificantBits());
            buf.putInt(ps.kills);
            buf.putInt(ps.deaths);
        }

        var res = new byte[buf.position()];
        System.arraycopy(buf.array(), 0, res, 0, res.length);
        return res;
    }

    public void dumpData(byte[] serialised) {
        var dataFolder = getDataFolder();
        if (!dataFolder.exists()) {
            dataFolder.mkdirs();
        }
        var dataFile = new File(dataFolder, dbFileName);
        try {
            Files.write(dataFile.toPath(), serialised);
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Failed to save data", e);
        }
    }

    public boolean loadData() {
        ByteBuffer buf;
        try {
            var dataFile = new File(getDataFolder(), dbFileName);
            if (!dataFile.exists()) {
                return true;
            }
            var bytes = Files.readAllBytes(dataFile.toPath());
            buf = ByteBuffer.wrap(bytes);
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Failed to load data", e);
            return false;
        }

        // @TODO(traks) handle exceptions?
        int dataVersion = buf.getInt();
        if (dataVersion == 0) {
            while (buf.hasRemaining()) {
                var uuid = new UUID(buf.getLong(), buf.getLong());
                var ps = new PlayerScores();
                ps.kills = buf.getInt();
                ps.deaths = buf.getInt();
                scoresMap.put(uuid, ps);
            }
            return true;
        } else if (dataVersion == 1) {
            newYearMode = (buf.getInt() != 0);
            while (buf.hasRemaining()) {
                var uuid = new UUID(buf.getLong(), buf.getLong());
                var ps = new PlayerScores();
                ps.kills = buf.getInt();
                ps.deaths = buf.getInt();
                scoresMap.put(uuid, ps);
            }
            return true;
        } else {
            getLogger().severe("Unknown database version " + dataVersion);
            return false;
        }
    }

    public void scheduleSaveData(boolean force) {
        if (busySavingToDB) {
            // previous save is taking a very long time
            getLogger().warning("Saving data is a taking a very long time");
            return;
        }

        // don't write to disk if nothing changed
        if (!dataChangedSinceLastSave && !force) {
            return;
        }
        dataChangedSinceLastSave = false;

        busySavingToDB = true;

        var serialised = serialiseData();

        Bukkit.getScheduler().runTaskAsynchronously(this, () -> {
            dumpData(serialised);
            busySavingToDB = false;
        });
    }

    public void removeFromSnowFightRegion(Player p, PlayerData pd) {
        if (pd.inSnowFightRegion) {
            // remove snowballs from inventory
            var inv = p.getInventory();
            for (int i = 0; i < inv.getSize(); i++) {
                if (snowballItem.isSimilar(inv.getItem(i))) {
                    inv.setItem(i, null);
                }
            }
            removeMarkedItems(p, bootsId);
        }

        pd.inSnowFightRegion = false;
    }

    public void removeFromKillZone(Player p, PlayerData pd) {
        if (pd.inKillZone) {
            p.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
            pd.objective = null;
            p.sendTitle(exitTitle, exitSubtitle, 10, 40, 10);

            removeMarkedItems(p, crossbowId);
            removeMarkedItems(p, fireworkId);
        }
        pd.inKillZone = false;
    }

    public void removeMarkedItems(Player p, NamespacedKey key) {
        var inv = p.getInventory();
        var contents = inv.getContents();
        for (int i = 0; i < contents.length; i++) {
            var is = contents[i];
            if (is != null && is.hasItemMeta() && is.getItemMeta().getPersistentDataContainer().has(key, PersistentDataType.BYTE)) {
                contents[i] = null;
            }
        }

        inv.setContents(contents);
    }

    public int countMarkedItems(Player p, NamespacedKey key) {
        var inv = p.getInventory();
        var contents = inv.getContents();
        int res = 0;
        for (int i = 0; i < contents.length; i++) {
            var is = contents[i];
            if (is != null && is.hasItemMeta() && is.getItemMeta().getPersistentDataContainer().has(key, PersistentDataType.BYTE)) {
                res += is.getAmount();
            }
        }
        return res;
    }

    // actual mod for n > 0
    public static int mod(int x, int n) {
        int res = (x % n); // may be negative
        // add n if res is negative (in which case top bit is 1 and we shift
        // right by sign extension)
        return ((res >> 31) & n) + res;
    }

    public ItemStack createSpecialFirework(Player p) {
        var res = new ItemStack(Material.FIREWORK_ROCKET);
        var im = (FireworkMeta) res.getItemMeta();

        im.getPersistentDataContainer().set(fireworkId, PersistentDataType.BYTE, (byte) 0);

        im.setPower(0);

        // customise firework effect based on player name

        int playerNameHash = p.getName().hashCode();
        var b = FireworkEffect.builder();
        var types = FireworkEffect.Type.values();
        b.with(types[mod(playerNameHash, types.length)]);
        b.withColor(Color.fromRGB(playerNameHash & 0xffffff));
        im.addEffect(b.build());

        res.setItemMeta(im);
        return res;
    }

    public void fillStack(ItemStack toFill, ItemStack with) {
        if (with.isSimilar(toFill) && toFill.getAmount() < toFill.getMaxStackSize()) {
            int add = Math.min(toFill.getMaxStackSize() - toFill.getAmount(), with.getAmount());
            toFill.setAmount(toFill.getAmount() + add);
            with.setAmount(with.getAmount() - add);
        }
    }

    public void properlyAddToInventory(Player p, ItemStack is, boolean prioritiseOffhand) {
        var inv = p.getInventory();
        var contents = inv.getContents();

        // first try to fill partial stacks

        int offhandIndex = 40;
        if (prioritiseOffhand) {
            fillStack(contents[offhandIndex], is);
            if (is.getAmount() == 0) {
                inv.setContents(contents);
                return;
            }
        }

        for (int i = 0; i < contents.length; i++) {
            if (i >= 36 && i <= 39) {
                // armour slots
                continue;
            }

            fillStack(contents[i], is);
            if (is.getAmount() == 0) {
                inv.setContents(contents);
                return;
            }
        }

        // add the remaining stack to an empty slot in the inventory

        if (prioritiseOffhand && (contents[offhandIndex] == null || contents[offhandIndex].getType() == Material.AIR)) {
            contents[offhandIndex] = is;
            inv.setContents(contents);
            return;
        }

        for (int i = 0; i < contents.length; i++) {
            if (i >= 36 && i <= 39) {
                // armour slots
                continue;
            }

            if (contents[i] == null || contents[i].getType() == Material.AIR) {
                contents[i] = is;
                inv.setContents(contents);
                return;
            }
        }
    }

    public void updateBoots(Player p, PlayerScores ps) {
        var boots = new ItemStack(Material.LEATHER_BOOTS);
        var im = (LeatherArmorMeta) boots.getItemMeta();
        im.setUnbreakable(true);
        im.addItemFlags(ItemFlag.values());
        // NOTE(traks): Remove lore entries such as "When on Body"
        im.setAttributeModifiers(HashMultimap.create());

        if (ps.kills >= 10000) {
            im.setColor(Color.AQUA);
            im.setEnchantmentGlintOverride(true);
            im.setDisplayName(ChatColor.AQUA + "Noob Tramplers");
        } else if (ps.kills >= 5000) {
            im.setColor(Color.PURPLE);
        } else if (ps.kills >= 1000) {
            im.setColor(Color.FUCHSIA);
        } else if (ps.kills >= 500) {
            im.setColor(Color.RED);
        } else if (ps.kills >= 100) {
            im.setColor(Color.ORANGE);
        } else if (ps.kills >= 50) {
            im.setColor(Color.YELLOW);
        } else if (ps.kills >= 20) {
            im.setColor(Color.LIME);
        } else if (ps.kills >= minKillsForAdvancement) {
            im.setColor(Color.GREEN);
        }

        // @NOTE(traks) add marker to the boots, so we don't edit other boots
        im.getPersistentDataContainer().set(bootsId, PersistentDataType.BYTE, (byte) 0);

        boots.setItemMeta(im);

        var curBoots = p.getInventory().getBoots();
        if (curBoots == null || !boots.isSimilar(curBoots) && curBoots.getItemMeta().getPersistentDataContainer().has(bootsId, PersistentDataType.BYTE)) {
            p.getInventory().setBoots(boots);
        }
    }

    @EventHandler
    public void onTick(ServerTickEndEvent e) {
        for (var p : Bukkit.getOnlinePlayers()) {
            var pd = playerData.get(p);

            if (Bukkit.getCurrentTick() - pd.joinTick <= 2) {
                // @NOTE(traks) currently CKEssentials clears the inventory of
                // players using a console command on join. It seems this
                // command doesn't fire immediately, so we need to a wait a few
                // ticks before giving the player snowballs if they join in a
                // snowfight region. Otherwise, all their snowballs will be
                // removed immediately!
                continue;
            }

            var localPlayer = WorldGuardPlugin.inst().wrapPlayer(p);
            var query = WorldGuard.getInstance().getPlatform().getRegionContainer().createQuery();

            if (p.getGameMode() != GameMode.SURVIVAL && p.getGameMode() != GameMode.ADVENTURE) {
                removeFromSnowFightRegion(p, pd);
                removeFromKillZone(p, pd);
                continue;
            }

            var ps = scoresMap.getOrDefault(p.getUniqueId(), new PlayerScores());

            if (!query.testState(localPlayer.getLocation(), localPlayer, snowFightFlag)) {
                // not in snow fight region
                removeFromSnowFightRegion(p, pd);
            } else {
                // player is currently in snow fight region
                if (!pd.inSnowFightRegion) {
                    // player wasn't in snow fight region before, i.e. they
                    // entered a snow fight region

                    removeMarkedItems(p, snowballId);

                    var give = snowballItem.clone();
                    give.setAmount(16 - pd.streakLength);
                    properlyAddToInventory(p, give, false);
                }

                updateBoots(p, ps);

                // give snowball again after a short timeout
                while (pd.popThrow()) {
                    if (!p.getInventory().containsAtLeast(snowballItem, 16)) {
                        var give = snowballItem.clone();
                        give.setAmount(1);
                        properlyAddToInventory(p, give, false);
                    }
                }

                pd.inSnowFightRegion = true;

                var spawnLoc = query.queryValue(localPlayer.getLocation(), localPlayer, snowFightSpawnFlag);
                if (spawnLoc != null) {
                    pd.lastRespawnLoc.copyLocation(spawnLoc);
                } else {
                    pd.lastRespawnLoc.copyLocation(p.getWorld().getSpawnLocation());
                }
            }

            if (query.testState(localPlayer.getLocation(), localPlayer, snowFightKillZoneFlag)) {
                // in kill zone, show additional info
                if (!pd.inKillZone) {
                    // player is currently in kill zone, but wasn't before.
                    // i.e. they are entering the kill zone

                    pd.killZoneEnterTick = Bukkit.getCurrentTick();
                    p.sendTitle(enterTitle, enterSubtitle, 10, 40, 10);

                    var sb = Bukkit.getScoreboardManager().getNewScoreboard();
                    var objective = sb.registerNewObjective("snowfight",
                            "dummy", ChatColor.AQUA + "The Blizzard", RenderType.INTEGER);
                    objective.setDisplaySlot(DisplaySlot.SIDEBAR);
                    objective.getScore("Hits").setScore(ps.kills);
                    objective.getScore("Losses").setScore(ps.deaths);
                    p.setScoreboard(sb);
                    pd.objective = objective;

                    removeMarkedItems(p, crossbowId);
                }

                pd.inKillZone = true;

                pd.objective.getScore("Hits").setScore(ps.kills);
                pd.objective.getScore("Losses").setScore(ps.deaths);

                p.setWalkSpeed(0.2f);

                if (newYearMode) {
                    // crossbows are only available in the blizzard
                    if (countMarkedItems(p, crossbowId) == 0) {
                        properlyAddToInventory(p, crossbowItem.clone(), false);
                    }

                    // give fireworks for crossbow
                    if (Bukkit.getCurrentTick() >= pd.lastFireworkGiveTick + 20 * 20) {
                        pd.lastFireworkGiveTick = Bukkit.getCurrentTick();

                        if (countMarkedItems(p, fireworkId) < 3) {
                            var add = createSpecialFirework(p);
                            properlyAddToInventory(p, add, true);
                        }
                    }
                } else {
                    removeMarkedItems(p, crossbowId);
                    removeMarkedItems(p, fireworkId);
                }
            } else if (pd.inKillZone) {
                pd.killZoneLeaveTick = Bukkit.getCurrentTick();
                removeFromKillZone(p, pd);
            }
        }

        var snowmenIter = spawnedSnowmen.entrySet().iterator();
        while (snowmenIter.hasNext()) {
            var entry = snowmenIter.next();
            var sm = entry.getKey();
            int spawnTick = entry.getValue();
            if (Bukkit.getCurrentTick() - spawnTick >= 20 * 20) {
                if (sm.isValid()) {
                    // spawn some particles while removing the snowman
                    var loc = sm.getLocation();
                    loc.getWorld().spawnParticle(Particle.SPIT, loc.add(0, 0.4, 0), 30, 0.2, 0.6, 0.2, 0, null, false);
                }
                sm.remove();
                snowmenIter.remove();
            }
        }

        // save data every so often
        if (Bukkit.getCurrentTick() % (5 * 60 * 20) == 0) {
            scheduleSaveData(false);
        }
    }

    @EventHandler
    public void onFire(EntityShootBowEvent e) {
        // unfortunately, the firework entity shot by the bow does not
        // inherit any of the tags from the PersistentDataContainer of
        // the crossbow or firework items... so this is a workaround.

        // FIXME: these checks may be extremely overkill.
        var proj = e.getProjectile();
        var ent = e.getEntity();

        // only allow fireworks shot by players
        if (!(proj instanceof Firework) || !(ent instanceof Player)) {
            return;
        }

        var bow = e.getBow();
        var fwItem = e.getConsumable();
        var fw = (Firework) proj;
        var pd = playerData.get((Player) ent);

        // make sure that this player is a snowfight player
        if (pd == null || !pd.inKillZone) {
            return;
        }

        var fwMeta = fwItem.getItemMeta();
        var bowMeta = bow.getItemMeta();

        if (fwMeta == null || bowMeta == null) {
            return;
        }

        // make sure that the bow has the right tag
        if (!bowMeta.getPersistentDataContainer().has(crossbowId, PersistentDataType.BYTE)) {
            return;
        }

        // make sure that the firework has the right tag
        if (!fwMeta.getPersistentDataContainer().has(fireworkId, PersistentDataType.BYTE)) {
            return;
        }

        // if everything is correct, we will endorse this firework
        fw.getPersistentDataContainer().set(fireworkId, PersistentDataType.BYTE, (byte) 0);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onThrow(PlayerLaunchProjectileEvent e) {
        var pd = playerData.get(e.getPlayer());
        if (pd != null && snowballItem.isSimilar(e.getItemStack())) {
            pd.pushThrow();
            snowballData.put((Snowball) e.getProjectile(),
                    new SnowballData(pd.inKillZone));
            // remove snowball if it somehow ends up in unloaded chunks. This
            // way when the snowball gets evicted from our snowball data map,
            // we're also sure it isn't physically in the world anymore
            e.getProjectile().setPersistent(false);
        }
    }

    public void setNewYearMode(boolean newMode) {
        if (newYearMode != newMode) {
            dataChangedSinceLastSave = true;
            newYearMode = newMode;
        }
    }

    public void updateScores(Player hitter, Player hit) {
        scoresMap.computeIfAbsent(hit.getUniqueId(), k -> new PlayerScores()).deaths++;
        var hitterPs = scoresMap.computeIfAbsent(hitter.getUniqueId(), k -> new PlayerScores());
        hitterPs.kills++;
        dataChangedSinceLastSave = true;

        if (hitterPs.kills == minKillsForAdvancement) {
            // grant kills advancement
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "advancement grant " + hitter.getName() + " only cubekrowd:christmas2021/games/snowball_fight");
        }
    }

    @EventHandler
    public void onHit(ProjectileHitEvent e) {
        var hit = e.getHitEntity();
        if (!(hit instanceof Player)) {
            return;
        }
        var p = (Player) hit;
        var pd = playerData.get(p);
        // @TODO(traks) here and below there's a small issue, where if a player
        // dies and respawns outside the kill zone, they can be killed again
        // outside the kill zone, until the exit delay passes.
        if ((!pd.inSnowFightRegion || !pd.inKillZone) && Bukkit.getCurrentTick() - pd.killZoneLeaveTick > killZoneExitDelay) {
            return;
        }
        var killer = e.getEntity().getShooter();
        if (!(killer instanceof Player) || killer.equals(p)) {
            return;
        }
        var killerP = (Player) killer;

        var ent = e.getEntity();
        if (ent instanceof Snowball) {
            var extra = snowballData.get(e.getEntity());
            if (extra == null || !extra.shotFromKillZone) {
                return;
            }
        } else {
            return;
        }

        var deathLoc = p.getLocation();

        // NOTE(traks): In 1.21 (or even earlier), we can't teleport players
        // with passengers, it won't even dismount the passengers. We got some
        // lobby perks going on that allow players to ride other players, so
        // dismount those first before teleporting.
        for (var passenger : p.getPassengers()) {
            p.removePassenger(passenger);
        }

        p.teleport(pd.getRespawnLocation(p));

        updateScores(killerP, p);

        var deathMsgId = random.nextInt(snowballDeathMessages.size());
        var deathMsg = snowballDeathMessages.get(deathMsgId);
        deathMsg = MessageFormat.format(deathMsg, p.getName(), killerP.getName());

        for (var onlineP : Bukkit.getOnlinePlayers()) {
            var onlinePd = playerData.get(onlineP);
            if (onlinePd.inKillZone) {
                onlineP.sendMessage(deathMsg);
            }
        }

        var snowman = (Snowman) deathLoc.getWorld().spawnEntity(deathLoc, EntityType.SNOW_GOLEM, CreatureSpawnEvent.SpawnReason.CUSTOM, entity -> {
            var sm = (Snowman) entity;
            sm.setInvulnerable(true);
            sm.setPersistent(false);
            sm.setDerp(true);
            sm.setCollidable(false);
            // disable pathfinding AI and stuff, gravity still applied
            sm.setAware(false);
        });
        spawnedSnowmen.put(snowman, Bukkit.getCurrentTick());

        p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, SoundCategory.NEUTRAL, 1, 1);
    }

    @EventHandler
    public void onFireworkDamage(EntityDamageByEntityEvent e) {
        var hit = e.getEntity();
        if (!(hit instanceof Player)) {
            return;
        }
        var p = (Player) hit;

        var damager = e.getDamager();
        if (!(damager instanceof Firework)) {
            return;
        }
        var fw = (Firework) damager;
        var killer = fw.getShooter();
        if (!(killer instanceof Player)) {
            return;
        }
        var killerP = (Player) killer;

        if (!fw.getPersistentDataContainer().has(fireworkId, PersistentDataType.BYTE)) {
            return;
        }

        e.setCancelled(true);

        var pd = playerData.get(p);
        if ((!pd.inSnowFightRegion || !pd.inKillZone) && Bukkit.getCurrentTick() - pd.killZoneLeaveTick > killZoneExitDelay) {
            return;
        }
        if (killerP.equals(p)) {
            return;
        }

        p.teleport(pd.getRespawnLocation(p));

        updateScores(killerP, p);

        var deathMsgId = random.nextInt(fireworkDeathMessages.size());
        var deathMsg = fireworkDeathMessages.get(deathMsgId);
        deathMsg = MessageFormat.format(deathMsg, p.getName(), killerP.getName());

        for (var onlineP : Bukkit.getOnlinePlayers()) {
            var onlinePd = playerData.get(onlineP);
            if (onlinePd.inKillZone) {
                onlineP.sendMessage(deathMsg);
            }
        }

        p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_HURT, SoundCategory.NEUTRAL, 1, 1);
    }

    // @NOTE(traks) block damage to non-player entities by weapons in the
    // snowfight game. E.g. snowballs can be used to remove items from item
    // frames.
    @EventHandler
    public void onNonPlayerDamage(EntityDamageByEntityEvent e) {
        var hit = e.getEntity();
        if (hit instanceof Player) {
            return;
        }
        var damager = e.getDamager();
        var data = damager.getPersistentDataContainer();
        if (data.has(fireworkId, PersistentDataType.BYTE) || data.has(snowballId, PersistentDataType.BYTE)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onHangingDamage(HangingBreakByEntityEvent e) {
        var damager = e.getRemover();
        if (damager != null) {
            var data = damager.getPersistentDataContainer();
            if (data.has(fireworkId, PersistentDataType.BYTE) || data.has(snowballId, PersistentDataType.BYTE)) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        var p = e.getPlayer();
        var pd = new PlayerData();
        pd.joinTick = Bukkit.getCurrentTick();
        playerData.put(p, pd);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        var p = e.getPlayer();
        var pd = playerData.remove(p);
        removeFromSnowFightRegion(p, pd);
        removeFromKillZone(p, pd);
    }
}
