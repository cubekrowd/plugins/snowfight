package net.cubekrowd.snowfight;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;

public class PlayerData {
    public int[] throwTickBuffer = new int[16];
    public int streakStartIndex;
    public int streakLength;

    public int lastFireworkGiveTick;

    public boolean inKillZone;
    public boolean inSnowFightRegion;
    public int killZoneEnterTick;
    public int killZoneLeaveTick;
    public Objective objective;

    public int joinTick;

    public OfflineLocation lastRespawnLoc = new OfflineLocation();

    public void pushThrow() {
        if (streakLength >= throwTickBuffer.length) {
            return;
        }
        int nextIndex = (streakStartIndex + streakLength) % throwTickBuffer.length;
        throwTickBuffer[nextIndex] = Bukkit.getCurrentTick();
        streakLength++;
    }

    public boolean popThrow() {
        if (streakLength == 0) {
            return false;
        }
        int firstThrowTick = throwTickBuffer[streakStartIndex];
        if (Bukkit.getCurrentTick() - firstThrowTick < 5 * 20) {
            return false;
        }
        streakStartIndex = (streakStartIndex + 1) % throwTickBuffer.length;
        streakLength--;
        return true;
    }

    public Location getRespawnLocation(Player p) {
        if (lastRespawnLoc.worldName == null) {
            return p.getWorld().getSpawnLocation();
        }

        var world = Bukkit.getWorld(lastRespawnLoc.worldName);

        if (world == null) {
            return p.getWorld().getSpawnLocation();
        }

        return lastRespawnLoc.toLocation();
    }
}
